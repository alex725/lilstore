import React from 'react';
import { Button } from 'semantic-ui-react'

const Register = () => {
    return (
    <div>
    <h1>Register and share your products with your audience.</h1>
    <Button color="white">Continue with email</Button>
    <Button color="white">Continue with Google</Button>
    </div>
    )
}

export default Register; 