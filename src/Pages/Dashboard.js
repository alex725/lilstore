import React from 'react'
import { Container, Button, Image } from 'semantic-ui-react'
import "./CssPages/Dashboard.css"
import RegisterHome from "../Modals/RegisterHome"
import EditProfie from '../Modals/EditProfile'
import NewProduct from "../Modals//NewProduct"

const Dashboard = () =>{

  return (
    <>
    <div  className="center"  >
    <Container >
    <h1> 
      Support the creators <br/>
      you love, by shopping<br/>
      from ther lists.
    </h1>
  <h4>
    Introducing lilStores - An experiment from Coolhumans<br/>
    Studio to help creators
    monetize from their product curation.
  </h4>
<Button>Explore Now</Button>
<RegisterHome />
<EditProfie />
<NewProduct/>
  </Container>
  </div>
  <div>
<Image src=""/>
  </div>
  </>
  )
}

export default Dashboard;