import React from 'react'
import { Button,  Image, Modal , Form, TextArea} from 'semantic-ui-react'
import UserPhoto from "../Images/Userphoto.png"
import "../App.css"

function NewProduct() {
  const [open, setOpen] = React.useState(false)

  return (
    <div >
    <Modal
       size="small"
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={  <Button className="button"    color='blue'>New Product</Button>}
      className="Logo"
    >
          <Modal.Content image>
          <Image   size='medium'  rounded  src={UserPhoto}  />
    <Form className="EditUserModal"  >
      <Modal.Header>Add a new product</Modal.Header>
       <Modal.Actions className="ButonModal" >
      <Button color='black' onClick={() => setOpen(false)}>
          Nope!
        </Button>
        <Button
          content="Save"
          labelPosition='right'
          icon='checkmark'
          onClick={() => setOpen(false)}
          positive
        />
          </Modal.Actions>
      <Form.Field >
      <label  >Name of the product</label>
      <input placeholder='Name of the product' />
    </Form.Field>
    <Form.Field  >
      <label  >Name Of the brand</label>
      <input placeholder='Name Of the brand' />
    </Form.Field>
    <Form.Field  >
      <label  >Cost</label>
      <input placeholder='$ 0.00' />
    </Form.Field>
    <Form.Field >
      <label  >Producto link</label>
      <input placeholder='Link to the product' />
    </Form.Field>

  </Form>
      </Modal.Content>
      <Modal.Actions className="ButonModal" >

      </Modal.Actions>
    </Modal>
    </div>
    
  )
}

export default NewProduct
