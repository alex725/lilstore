import React from 'react'
import { Button,  Image, Modal , Form, TextArea} from 'semantic-ui-react'
import UserPhoto from "../Images/Userphoto.png"
import "../App.css"

function EditProfie() {
  const [open, setOpen] = React.useState(false)

  return (
    <div >
    <Modal
       size="small"
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={  <Button className="button"    color='blue'>Edit Profile</Button>}
      className="Logo"
    >
          <Modal.Content image>
    <Form className="EditUserModal"  >
      <Modal.Header>Edit your profile</Modal.Header>
      <Image  className="EditUserModal"  src={UserPhoto} size='small' circular  />
      <Button primary>Upload a picture
</Button>
      <Form.Field labelPosition='right' >
      <label  >Name</label>
      <input placeholder='First Name' />
    </Form.Field>
    <Form.Field labelPosition='right' >
      <label  >Username</label>
      <input placeholder='@Username' />
    </Form.Field>
    <Form.Field labelPosition='left' >
      <label  >Instagram profile</label>
      <input placeholder='Instagram profile' />
    </Form.Field>
    <label  >Tell us more</label>
 <TextArea placeholder='Write a short bio'  />
  </Form>
      </Modal.Content>
      <Modal.Actions className="ButonModal" >
        <Button color='black' onClick={() => setOpen(false)}>
          Nope!
        </Button>
        <Button
          content="Save"
          labelPosition='right'
          icon='checkmark'
          onClick={() => setOpen(false)}
          positive
        />
      </Modal.Actions>
    </Modal>
    </div>
    
  )
}

export default EditProfie
