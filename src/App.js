import './App.css';
import 'semantic-ui-css/semantic.min.css'
import Dashboard from "./Pages/Dashboard"
import RegisterEmail from "./Pages/RegisterEmail"
import Register from "./Pages/Register"
import Profile from "./Pages/ProfilePages/Profile"
import ProfileAdd from "./Pages/ProfilePages/ProfileAdd"
import ProfileComplete from "./Pages/ProfilePages/ProfileComplete"
import ProfileEmpty from "./Pages/ProfilePages/ProfileEmpty"
import { BrowserRouter as Router, Route } from "react-router-dom";


function AppRouter(){
 return (
   <Router>
  <div className={"site-content"}>
    <Route path="/" exact component={Dashboard} />
    <Route path="/RegisterEmail/" component={RegisterEmail} />
    <Route path="/Register/"  component={Register} />
    <Route path="/Profile/"  component={Profile} />
    <Route path="/ProfileAdd/"  component={ProfileAdd} />
    <Route path="/ProfileComplete/"  component={ProfileComplete} />
    <Route path="/ProfileEmpty/"  component={ProfileEmpty} />
</div>
</Router>
  );
}
 

export default AppRouter;
